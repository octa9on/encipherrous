I needed to encrypt backups before uploading to cloud storage, but I didn't want to leave a secret key in a script, so I wrote this little tool to encrypt files with a public key, which can later be decrypted with the private key.

```
Usage:
enc --keypair
    create public+private keypair

enc --encrypt PUBLIC_KEY FILE
    encrypt FILE with PUBLIC_KEY

enc --decrypt FILE
    decrypt FILE with private key
```
