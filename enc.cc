#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

extern "C" {
#include <sodium.h>
}

using namespace std;

void die(string message) {
    cout << message << endl;
    exit(1);
}

void usage() {
    cout << "Usage:" << endl;
    cout << "enc --keypair" << endl;
    cout << "    create public+private keypair" << endl;
    cout << endl;
    cout << "enc --encrypt PUBLIC_KEY FILE" << endl;
    cout << "    encrypt FILE with PUBLIC_KEY" << endl;
    cout << endl;
    cout << "enc --decrypt FILE" << endl;
    cout << "    decrypt FILE with private key" << endl;
    cout << endl;

    exit(0);
}

string input(string prompt) {
    cout << prompt;
    string line;
    getline(cin, line);
    return line;
}

string readfile(string filename) {
    ifstream infile(filename, ios::binary);

    infile.seekg(0, ios::end);
    size_t size = infile.tellg();
    if (size < 0) die("can't open file");
    infile.seekg(0, ios::beg);
    char * bytes = (char *) malloc(size);
    infile.read(bytes, size);

    string contents(bytes, size);
    free(bytes);
    return contents;
}

const string D16 = "01234567890abcdef";

int main(int nargs, char * args[]) {
    if (sodium_init() < 0) die("can't sodium");

    if (nargs == 2 && string(args[1]) == "--keypair") {
        uint8_t recipient_pubkey[crypto_box_PUBLICKEYBYTES];
        uint8_t recipient_seckey[crypto_box_SECRETKEYBYTES];
        crypto_box_keypair(recipient_pubkey, recipient_seckey);

        cout << "public key: ";
        cout << hex;
        for (int ix=0 ; ix<crypto_box_PUBLICKEYBYTES ; ix+=1) {
            cout << setfill('0') << setw(2) << int(recipient_pubkey[ix]);
        }
        cout << dec << endl;

        cout << "private key: ";
        cout << hex;
        for (int ix=0 ; ix<crypto_box_SECRETKEYBYTES ; ix+=1) {
            cout << setfill('0') << setw(2) << int(recipient_seckey[ix]);
        }
        cout << dec << endl;
    }
    else if (nargs == 4 && string(args[1]) == "--encrypt") {
        string raw_key = args[2];
        if (raw_key.length() != crypto_box_PUBLICKEYBYTES*2) die("bad key");
        if (raw_key.find_first_not_of(D16) != string::npos) die("bad key");

        uint8_t key[crypto_box_PUBLICKEYBYTES];
        int problem = sodium_hex2bin(
            key, crypto_box_PUBLICKEYBYTES,
            raw_key.c_str(), raw_key.length(),
            nullptr, nullptr, nullptr);
        if (problem != 0) die("bad key");

        string filename = args[3];
        string plaintext = readfile(filename);

        int ciphertext_length = crypto_box_SEALBYTES + plaintext.length();
        uint8_t * ciphertext = (uint8_t *) malloc(ciphertext_length);
        crypto_box_seal(ciphertext, reinterpret_cast<const uint8_t *>(plaintext.c_str()), plaintext.length(), key);

        ofstream outfile(filename+".enc", ios::binary);
        for (int ix=0 ; ix<ciphertext_length ; ix+=1) outfile << ciphertext[ix];
        outfile.close();
        free(ciphertext);
    }
    else if (nargs == 3 && string(args[1]) == "--decrypt") {
        string filename = args[2];
        string ciphertext = readfile(filename);
        int problem;

        string sec_line = input("private key (hex): ");
        if (sec_line.length() != crypto_box_SECRETKEYBYTES*2) die("bad key");
        if (sec_line.find_first_not_of(D16) != string::npos) die("bad key");
        uint8_t sec_key[crypto_box_SECRETKEYBYTES];
        problem = sodium_hex2bin(
            sec_key, crypto_box_SECRETKEYBYTES,
            sec_line.c_str(), sec_line.length(),
            nullptr, nullptr, nullptr);
        if (problem != 0) die("bad key");

        uint8_t pub_key[crypto_box_PUBLICKEYBYTES];
        crypto_scalarmult_curve25519_base(pub_key, sec_key);

        int plaintext_length = ciphertext.length() - crypto_box_SEALBYTES;
        uint8_t * plaintext = (uint8_t *) malloc(plaintext_length);
        problem = crypto_box_seal_open(plaintext, reinterpret_cast<const uint8_t *>(ciphertext.c_str()), ciphertext.length(), pub_key, sec_key);
        if (problem != 0) die("bad decrypt");

        ofstream outfile(filename.substr(0,filename.length()-4), ios::binary);
        for (int ix=0 ; ix<plaintext_length ; ix+=1) outfile << plaintext[ix];
        outfile.close();
        free(plaintext);
    }
    else usage();
}
